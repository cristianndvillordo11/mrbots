from django.shortcuts import render
from productos.models import Productos, Categorias
from django.core.paginator import Paginator
from productos.forms import SearchForm

def index(request):
	#BUSQUEDA--------------------------------------------------------------------------------------------------------
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	#PAGINACION-----------------------------------------------------------
	lista_productos = Productos.objects.all().order_by( "-fecha_creacion")
	paginate = Paginator(lista_productos, 3)
	page_number = request.GET.get('page')
	page_obj = paginate.get_page(page_number)

	lista_productos = Productos.objects.all()[0:7]
	lista_categorias = Categorias.objects.all()
	form = SearchForm()
	contexto ={
		'form':form,
		'search_form':search_form,	
		"page_obj" : page_obj,
		"lista_categorias": lista_categorias,
		'lista_productos': lista_productos,
		}
	return render(request,"index.html", contexto)


def acercade(request):
    return render(request, 'sobre_nosotros.html', {})

