from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import logout,login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from .forms import ProductosForm, SearchForm, CarritoForm
from .models import Productos, Categorias, Carritos
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import PasswordChangeForm
from django.core.paginator import Paginator
from django.utils import timezone
from django.contrib.auth.models import  User
#para enviar gmail
from django.conf import settings
from django.template.loader import get_template # template visto desde el gmail
from django.core.mail import EmailMultiAlternatives

# Create your views here.
@login_required
def crear_producto(request):
	#BUSQUEDA--------------------------------------------------------------------------------------------------------
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	
	#CREAR PRODUCTO---------------------------------------------------------------------------------------------------
	if request.method == "POST":
		form= ProductosForm(request.POST,request.FILES)
		if form.is_valid():
			producto=form.save(commit=False)
			producto.usuario = request.user
			producto.save()
			return redirect("productos:ver_productos", producto.id)
		else:
			contexto={"form":form,
					"search_form":search_form,
			}
			return render(request, "productos/crearProductos.html",contexto)
	form = ProductosForm()
	productos : Productos.objects.all()
	contexto={"form":form,
			"search_form":search_form,
			}
	return render(request, "productos/crearProductos.html",contexto)

def ver_productos(request, id):
	#BUSQUEDA--------------------------------------------------------------------------------------------------------
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	#ver productos----------------------------------------------------------------------------------------------------
	producto = get_object_or_404(Productos,id=id)
	#PAGINACION-----------------------------------------------------------
	productos= Productos.objects.all().order_by("-fecha_creacion")[0:10]
	paginate = Paginator(productos, 3 )
	page_number = request.GET.get('page')
	page_obj = paginate.get_page(page_number)
	#agregar Carrito----------------------------------------------------------------
	form_carrito = CarritoForm()
	carritos = Carritos.objects.order_by("-fecha_creacion")
	lista_categorias = Categorias.objects.all()
	form = SearchForm()
	contexto ={
		"producto" : producto,		
		'form':form,
		'search_form':search_form,	
		"page_obj" : page_obj,
		"lista_categorias": lista_categorias,
		'carritos':carritos,
		'form_carrito': form_carrito,
		}
	return render(request,"productos/ver_productos.html", contexto)

@login_required
def borrarProducto(request, id):
  	producto = get_object_or_404(Productos, id=id)
  	producto.delete()
  	return redirect("index")
@login_required
def editar_producto(request, id):
    producto = get_object_or_404(Productos, id=id)
    if request.method == "POST":  
        user = User.objects.get(username=request.user)   
        producto.usuario = user
        form = ProductosForm(data=request.POST, files=request.FILES, instance=producto)
        if form.is_valid():
            form.save()
            return redirect("index")
    else:
        form = ProductosForm(instance = producto)
        return render(request, 'productos/editar_producto.html', {
            "producto": producto,
            "form": form
        })

def search(request):
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()

	filtro_titulo = request.GET.get("titulo", "")
	orden_post = request.GET.get("orden", None)
	lista_productos = Productos.objects.filter(titulo__icontains = filtro_titulo)

	if orden_post == "titulo":
		lista_productos= lista_productos.order_by("titulo")
	elif orden_post == "antiguo":
		lista_productos= lista_productos.order_by("fecha_creacion")
	elif orden_post == "nuevo":
		lista_productos= lista_productos.order_by("-fecha_creacion")

	#paginate
	
	paginate = Paginator(lista_productos, 12)
	page_number = request.GET.get('page')
	page_obj = paginate.get_page(page_number)

	form = SearchForm()
	lista_categorias = Categorias.objects.all()		
	contexto = { "form":form,
				 "lista_productos":lista_productos,
				 "search_form":search_form,
				 "lista_categorias": lista_categorias,
				 "page_obj" : page_obj,
				 }
	return render(request, "productos/search.html",contexto)


def filtro_categoria(request, id):
    categoria = get_object_or_404(Categorias, id=id)
    queryset = Productos.objects.all()
    queryset = queryset.filter(categoria=categoria)
    lista_categorias = Categorias.objects.all()
    contexto = {
        "lista_productos": queryset,
        "lista_categorias": lista_categorias,
        "categoria_seleccionada": categoria,
        }
    return render(request,"index.html", contexto)
@login_required
def carrito(request):
	#BUSQUEDA--------------------------------------------------------------------------------------------------------
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	
	carritos = Carritos.objects.all()
	usuario = request.user
	#total de productos
	total= 0
	for x in carritos:
		if request.user == x.usuario:
			total+= int(x.producto.precio_oferta)
			total*= int(x.cantidad)
			form = CarritoForm()

	#modificar cantidad de productos en el carrito
	lista_categorias = Categorias.objects.all()
	form = CarritoForm()
	contexto ={
    "search_form":search_form,
	'carritos':carritos,
	'usuario':usuario,
	'total':total,
	'form_carrito':form,
	"lista_categorias": lista_categorias,
    }
	return render(request,"carritos/carritos.html", contexto)

@login_required
def agregarCarrito(request, id):
	if request.GET:
		search_form = SearchForm(request.GET)
	else:
		search_form = SearchForm()
	#FIN BUSQUEDA-----------------------------------------------------------------------------------------------------
	
	#agregar carrito----------------------------------------------------------------------------------------------------
	
	producto = Productos.objects.get(pk=id)
	if request.method == "POST":
		form = CarritoForm(request.POST)
		user = User.objects.get(username=request.user)
		if form.is_valid():
			carrito = form.save(commit=False)
			carrito.usuario = request.user
			carrito.producto = producto
			carrito.save()
			return redirect("productos:carrito")
	else:
		return redirect("ver_productos", producto.id)
@login_required
def borrar_carrito(request, id):
	carrito = get_object_or_404(Carritos, id=id)
	carrito.delete()
	return redirect("index")
@login_required
def editar_carrito(request, id):
    carrito = get_object_or_404(Carritos, id=id)
    if request.method == "POST":  
        user = User.objects.get(username=request.user)   
        carrito.usuario = user
        form = CarritoForm(data=request.POST, files=request.FILES, instance=carrito)
        if form.is_valid():
            form.save()
            return redirect("productos:carrito")
    else:
        form = CarritoForm(instance = carrito)
        return render(request, 'carritos/carritos.html', {
            "carrito": carrito,
            "form": form
        })


def send_email(mail, contenido):
	contexto = {'mail':mail, 'contenido':contenido}
	template = get_template('correo.html')
	content = template.render(contexto)

	email = EmailMultiAlternatives(
		'Correo Electronico', #titulo del correo
		'MrBots',  #mensaje que describe el correo
		settings.EMAIL_HOST_USER,#cuenta de correo utulizando 
		[mail],
		cc=['Mrbots.allsoftware@gmail.com']#lista de correos que se envia el contenido (destinatario)
		)
	email.attach_alternative(content, 'text/html')
	email.send()

def contacto(request):
	if request.method == 'POST':
		mail = request.POST.get('mail')
		contenido = request.POST.get('contenido')
		send_email(mail, contenido)
	contexto = {}
	return render(request, 'productos/contacto.html', contexto)

#modo backentd
#def send_email(mail, contenido):
#	contexto = {'mail':mail, 'contenido':contenido}
#	template = get_template('correo.html')
#	content = template.render(contexto)
#
#	email = EmailMultiAlternatives(
#		'Correo Electronico', #titulo del correo
#		'MrBots',  #mensaje que describe el correo
#		settings.EMAIL_HOST_USER,#cuenta de correo utulizando 
#		[mail],#lista de correos que se envia el contenido (destinatario)
#		)
#	email.attach_alternative(content, 'text/html')
#	email.send(content)

#def contacto(request):
#	if request.method == 'POST':
#		mail = request.POST.get('mail')
#		contenido = request.POST.get('contenido')
#		send_email(mail, contenido)
#	contexto = {}

#	return render(request, 'productos/contacto.html', contexto)



