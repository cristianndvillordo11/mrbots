from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm

from django.contrib.auth.models import User
from .models import Productos, Carritos

class ProductosForm(forms.ModelForm):
    class Meta:
        model = Productos
        fields = "__all__"
        exclude = ('usuario',)

    def __init__(self, *args, **kwargs):
        super(ProductosForm, self).__init__(*args, **kwargs)
        self.fields["categoria"].widget.attrs.update({'class': 'pub_categoria form-select','id':"floatingSelectGrid", 'aria-label': "Floating label select example"})
        self.fields["titulo"].widget.attrs.update({'class': 'form-control pub_titulo', 'id':"floatingInputGrid", 'placeholder':"titulo del producto", 'value':"escriba aquí el titulo del producto"})
        self.fields["precio"].widget.attrs.update({'class':"form-control", 'aria-label':"Amount (to the nearest dollar)"})
        self.fields["contenido"].widget.attrs.update({'class': 'form-control pub_titulo', 'id':"floatingInputGrid", 'placeholder':"contenido", 'value':"escriba el contenido del producto"})

        

class SearchForm(forms.Form):
    titulo = forms.CharField(max_length=30, required = False)
    ORDER_OPCIONES = (
        ("titulo", "Titulo"),
        ("Fecha",(
            ("antiguo", "Antiguo"),
            ("nuevo", "Nuevo"))
        ))
    orden = forms.ChoiceField(choices=ORDER_OPCIONES, required = False,
        initial="nuevo")

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields["titulo"].widget.attrs.update({"class":"form-control me-2", "type":"search", "placeholder":"Buscar", "aria-label":"Search"})


class CarritoForm(forms.ModelForm):
    class Meta:
        model = Carritos
        fields = ('cantidad',)

    def __init__(self, *args, **kwargs):
        super(CarritoForm, self).__init__(*args, **kwargs)
        self.fields["cantidad"].widget.attrs.update({'type':"number",'value':"{{'cantidad'}}",'class':"form-control md-2", 'id':"cantidad", 'name':"cantidad"})
