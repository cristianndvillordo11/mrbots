from django.urls import path, include
from . import views

app_name = "productos"
urlpatterns = [
    path('filtro_categoria/<int:id>', views.filtro_categoria, name="filtro_categoria"),
    path('crear_producto/', views.crear_producto, name="crear_producto"),
    path("productos/ver_productos/<int:id>", views.ver_productos, name='ver_productos'),
    path("productos/<int:id>/borrar", views.borrarProducto, name="borrarProducto"),
    path("productos/editar_producto/<int:id>", views.editar_producto, name="editar_producto"),
    path('carrito', views.carrito, name="carrito"),
    path('carrito/<int:id>/agregarCarrito', views.agregarCarrito, name="agregarCarrito"),
    path("productos/<int:id>/borrar_carrito", views.borrar_carrito, name="borrar_carrito"),
    path("productos/<int:id>/editar_carrito", views.editar_carrito, name="editar_carrito"),
    path("productos/search", views.search, name="search"),
    path("productos/contacto", views.contacto, name="contacto")
]